const puppeteer = require('puppeteer');
const fs = require('fs');


 async function get_page_list(page){
    console.log('Get Page List');
    const pager_link = page.evaluate(() =>{
        var pager_link = document.querySelector('.pager').querySelectorAll('a');
        pager_link.forEach(link => {
            console.log(link);
        });
        return "hello from query selector";

    });

    return "pager_link";
 }

(async () => {
    var main_url = 'https://www.glisshop.com/marque/lib-tech/';
    const browser = await puppeteer.launch({
        headless: true,
        slowMo: 250,
    });
    var main_page = await browser.newPage();
    await main_page.goto(main_url); 

    // Get all pages 
    var page_list = await main_page.evaluate(() =>{
        var pager_link = document.querySelector('.pager').querySelectorAll('a');
        var page_list = [];
        for(var link_index in pager_link){
            if(pager_link[link_index].href != undefined && page_list.indexOf(pager_link[link_index].href) == -1){
                page_list.push(pager_link[link_index].href);
            }
        }
        return page_list;    
    });
    await main_page.close();

    if (page_list.indexOf("") != -1){
        page_list[page_list.indexOf("")] = main_url;
    }

    // Loop on each page and get all products.
    var product_list = [];
    for (var page_index in page_list){
        var product_page = await browser.newPage();
        
        await product_page.goto(page_list[page_index]);
        var page_product_list = await product_page.evaluate(() => {
            var res = [];
            var product_list = document.querySelectorAll(".product-item");

            product_list.forEach(product_item => {
                var product_name = product_item.querySelector(".product-label_title").innerText;
                var product_url = product_item.querySelector(".product-label a").href;
                var product_price = product_item.querySelector(".product-price .main-price .price .price-value").innerText;
                product_price = product_price.replace('€', '').replace(',', '.').trim();
                product_price = parseFloat(product_price);

                if(product_item.querySelector('.product-price .product-old-prices .old-price') != null){
                    var product_old_price = product_item.querySelector('.product-price .product-old-prices .old-price .price-old-value').innerText;
                    product_old_price = product_old_price.replace('€', '').replace(',', '.').trim();
                    product_old_price = parseFloat(product_old_price);
                }
                else{
                    var product_old_price = product_price;
                }

                var size_list = [];

                res.push({
                    'name': product_name,
                    'url': product_url,
                    'price': product_price,
                    'old_price': product_old_price,
                })
            });
        
            return res
        });

        product_list = product_list.concat(page_product_list);
        await product_page.close()
    }

    await browser.close();

    fs.writeFileSync('product.json', JSON.stringify({'product_list': product_list}));
})();
